package cn.itcast.hotel.controller;

import cn.itcast.hotel.pojo.PageResult;
import cn.itcast.hotel.pojo.Parameter;
import cn.itcast.hotel.service.IHotelService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @auther zgzstart
 * @creat2022-03-23 time:15:26
 */
@RestController
@RequestMapping("hotel")
public class HotelController {
    @Resource
    private IHotelService iHotelService;

    //需求一根据搜索框的内容进行查询对应的匹配结果和进行分页以及排序;
    @PostMapping("/list")
    public PageResult getHotelsBySearch(@RequestBody Parameter parameter) {

        return null;

    }

    //浏览器页面有过滤条件的需求,而这些过滤条件不是写死的,二十根据你已经传入的信息来进行统计,统计出来的词条
    //而返回值需要一个map对象,进行键值对的分辨;
    @PostMapping("/filters")
    public Map getHotelFilter(@RequestBody Parameter parameter) {

        return null;
    }

    //来实现输入自动补全的功能;
    @GetMapping("/suggestion")
    public List<String> getSuggestion(@RequestParam String key){

     return null;

    }



}
