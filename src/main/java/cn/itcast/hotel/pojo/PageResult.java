package cn.itcast.hotel.pojo;

import lombok.Data;

import java.util.List;

/**
 * @auther zgzstart
 * @creat2022-03-23 time:15:44
 */
@Data
public class PageResult {
    private int total;
    private List<HotelDoc> hotels;
}
