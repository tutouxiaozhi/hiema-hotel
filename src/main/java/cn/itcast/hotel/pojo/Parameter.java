package cn.itcast.hotel.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @auther zgzstart
 * @creat2022-03-23 time:15:27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Parameter {
  private   String key;
  private Integer page;
  private Integer size;
  private String sortBy;
  private String brand;
  private String city;
  private String starName;
  private Integer maxPrice;
  private Integer minPrice;
  private String location;
}
